﻿using System;
using System.Diagnostics;
using UnityEngine;

/// <summary>
/// A collection of helper classes, enums and structs.
/// </summary>
public static class Helper
{
  public static int PerlinNoise(int x, int y, int z, float scale, float height, float power)
  {
    float rValue;
    rValue = Noise.Noise.GetNoise(((double)x) / scale, ((double)y) / scale, ((double)z) / scale);
    rValue *= height;

    if (power != 0)
    {
      rValue = Mathf.Pow(rValue, power);
    }

    return (int)rValue;
  }

  public static IntPosition World2StorageCoords(IntPosition coordsInWorld)
  {
    IntPosition coordsInStorage;
    coordsInStorage.x = Mathf.FloorToInt((float)coordsInWorld.x / Chunk.ChunkSize);
    coordsInStorage.y = Mathf.FloorToInt((float)coordsInWorld.y / Chunk.ChunkSize);
    coordsInStorage.z = Mathf.FloorToInt((float)coordsInWorld.z / Chunk.ChunkSize);
    return coordsInStorage;
  }

}


public enum Direction
{
  Up, Down, West, East, North, South
}


#region IntPosition

/// <summary>
/// Struct that represents integer 3D coordinates.
/// </summary>
[DebuggerDisplay("{x},{y},{z}")]
public struct IntPosition : IEquatable<IntPosition>
{

  /// <summary>
  /// Initializes by using only integer part of vector3.
  /// </summary>
  /// <param name="vector"></param>
  public IntPosition(Vector3 vector)
  {
    this.x = (int)vector.x;
    this.y = (int)vector.y;
    this.z = (int)vector.z;
  }
  public IntPosition(int x, int y, int z)
  {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  public int x, y, z;

  public bool Equals(IntPosition other)
  {
    if (x != other.x || y != other.y || z != other.z)
      return false;
    return true;
  }

  public static bool operator !=(IntPosition pos1, IntPosition pos2)
  {
    return !pos1.Equals(pos2);
  }

  public static bool operator ==(IntPosition pos1, IntPosition pos2)
  {
    return pos1.Equals(pos2);
  }
  public static IntPosition operator +(IntPosition pos1, IntPosition pos2)
  {
    return new IntPosition(pos1.x + pos2.x, pos1.y + pos2.y, pos1.z + pos2.z);
  }

  public static IntPosition operator -(IntPosition pos1, IntPosition pos2)
  {
    return new IntPosition(pos1.x - pos2.x, pos1.y - pos2.y, pos1.z - pos2.z);
  }

  public static IntPosition operator *(IntPosition pos1, int number)
  {
    return new IntPosition(pos1.x * number, pos1.y * number, pos1.z * number);
  }

  public Vector3 ToVector3()
  {
    return new Vector3(x, y, z);
  }

  public IntPosition Neighbour(Direction direction)
  {
    switch (direction)
    {
      case Direction.Up:
        return this.Up;
      case Direction.Down:
        return this.Down;
      case Direction.North:
        return this.North;
      case Direction.South:
        return this.South;
      case Direction.East:
        return this.East;
      case Direction.West:
        return this.West;
      default:
        throw new ArgumentException(string.Format("Unable to determine neighbouring coordinates: unknown direction {0}", direction.ToString()));
    }
  }

  public override string ToString()
  {
    return string.Format("[{0},{1},{2}]", x, y, z);
  }

  public IntPosition Up { get { return new IntPosition(this.x, this.y + 1, this.z); } }
  public IntPosition Down { get { return new IntPosition(this.x, this.y - 1, this.z); } }
  public IntPosition West { get { return new IntPosition(this.x - 1, this.y, this.z); } }
  public IntPosition East { get { return new IntPosition(this.x + 1, this.y, this.z); } }
  public IntPosition North { get { return new IntPosition(this.x, this.y, this.z + 1); } }
  public IntPosition South { get { return new IntPosition(this.x, this.y, this.z - 1); } }
}
#endregion