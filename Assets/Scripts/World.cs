﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
  [SerializeField]
  private GameObject chunkPrefab;
  [SerializeField]
  private GameObject player;
  private IntPosition PlayerPosition { get { return new IntPosition(player.transform.position); } }
  private Dictionary<IntPosition, Chunk> chunks = new Dictionary<IntPosition, Chunk>();
  private IntPosition centerChunk; // storage position of the chunk that is in the middle of rendered world
  public static int WorldHeight = 3;
  public static int WorldRadius = 8;

  private void Start()
  {
    centerChunk = new IntPosition(0, 0, 0);
    FillWithChunks();
  }

  private void Update()
  {
    //dynamic terrain generation turned off because of awfull lags
    //TODO: use a separate thread for loading chunks
    //LoadNewChunksIfNeeded();
  }


  /// <summary>
  /// Creates a chunk in a given world storage coordinates and initializes it.
  /// </summary>
  /// <param name="x"></param>
  /// <param name="y"></param>
  /// <param name="z"></param>
  public void AddChunk(int x, int y, int z)
  {
    //first transform the coordinates to corresponsind chunk coordinates
    IntPosition storagePosition = new IntPosition(x, y, z);
    IntPosition coordsInWorld = storagePosition * Chunk.ChunkSize;

    if (chunks.ContainsKey(storagePosition))
      throw new System.ArgumentException(string.Format("There is a chunk already covering the world coordinates at [{0},{1},{2}]", storagePosition.x, storagePosition.y, storagePosition.z));

    Vector3 coordsInGame;
    //shifting by 0.5 to make block centers to be in the whole numbers
    coordsInGame.x = coordsInWorld.x - 0.5f;
    coordsInGame.y = coordsInWorld.y + 0.5f;
    coordsInGame.z = coordsInWorld.z - 0.5f;

    GameObject newChunkObject = Instantiate(chunkPrefab, coordsInGame, new Quaternion(0, 0, 0, 0)) as GameObject;
    chunks[storagePosition] = newChunkObject.GetComponent<Chunk>();
    chunks[storagePosition].Init(this.GetComponent<World>(), coordsInWorld);
  }

  /// <summary>
  /// Get block at a given world integer coordinates.
  /// </summary>
  /// <param name="coordsInWorld">Position of a center of a block in the world</param>
  /// <returns>s</returns>
  public Block GetBlock(IntPosition coordsInWorld)
  {
    IntPosition coordsInStorage = Helper.World2StorageCoords(coordsInWorld);
    Chunk chunk;
    if (chunks.TryGetValue(coordsInStorage, out chunk))
    {
      return chunk.GetBlockFromChunk(coordsInWorld - chunk.coordsInWorld);
    }
    else
    {
      return null;
    }
  }

  /// <summary>
  /// Attempts to find chunk at given world coordinates. If no chunk is found, throws an exception.
  /// </summary>
  /// <param name="coordsInWorld"></param>
  /// <returns></returns>
  public Chunk GetChunk(IntPosition coordsInWorld)
  {
    IntPosition coordsInStorage = Helper.World2StorageCoords(coordsInWorld);
    Chunk foundChunk = null;
    if (!chunks.TryGetValue(coordsInStorage, out foundChunk))
      throw new System.ArgumentOutOfRangeException(string.Format("Unable to find chunk at coordinates {0}", coordsInWorld.ToString()));
    return foundChunk;
  }

  /// <summary>
  /// Tries to set a block of given type at given world coordinates.
  /// </summary>
  /// <param name="coordsInWorld"></param>
  /// <returns></returns>
  public void SetBlock(IntPosition coordsInWorld, Type blockType)
  {
    Chunk foundChunk = GetChunk(coordsInWorld);
    Block newBlock = Activator.CreateInstance(blockType, new object[] { foundChunk, coordsInWorld - foundChunk.coordsInWorld }) as Block;

    IntPosition coordsInStorage = Helper.World2StorageCoords(coordsInWorld);
    Chunk chunk;
    if (!chunks.TryGetValue(coordsInStorage, out chunk))
      throw new System.ArgumentOutOfRangeException("There is no chunk at given coordinates: {0}", coordsInWorld.ToString());

    IntPosition coordsInChunk = coordsInWorld - coordsInStorage * Chunk.ChunkSize;
    chunk.SetBlock(coordsInChunk, newBlock);
  }


  public void FillWithChunks()
  {
    for (int x = -WorldRadius; x < WorldRadius; x++)
    {
      for (int z = -WorldRadius; z < WorldRadius; z++)
      {
        CreateOrActivateChunkColumn(x, z);
      }
    }
  }

  /// <summary>
  /// Creates a terrain of rocks and grass using perlin noise.
  /// </summary>
  private void CreateTerrain()
  {
    for (int x = -WorldRadius + centerChunk.x; x < WorldRadius + centerChunk.x; x++)
    {
      for (int z = -WorldRadius + centerChunk.z; z < WorldRadius + centerChunk.z; z++)
      {
        CreateTerrainInColumn(x, z);
      }
    }
  }

  /// <summary>
  /// Creates terrain in a column at given chunk storage storage coordinates.
  /// </summary>
  /// <param name="storageX"></param>
  /// <param name="storageZ"></param>
  private void CreateTerrainInColumn(int storageX, int storageZ)
  {
    //coordinates of first block in the chunk (back bottom right)
    IntPosition startCoordsInWorld = new IntPosition(storageX * Chunk.ChunkSize, 0, storageZ * Chunk.ChunkSize);
    for (int x = startCoordsInWorld.x; x < startCoordsInWorld.x + Chunk.ChunkSize; x++)
    {
      for (int z = startCoordsInWorld.z; z < startCoordsInWorld.z + Chunk.ChunkSize; z++)
      {
        int rockLayer = Helper.PerlinNoise(x, 0, z, 100, 30, 1f);
        int grassLayer = Helper.PerlinNoise(x, 0, z, 30, 5, 1f);

        // create the bedrock
        SetBlock(new IntPosition(x, 0, z), typeof(BedrockBlock));

        for (int y = 1; y < WorldHeight * Chunk.ChunkSize; y++)
        {
          IntPosition coordsInWorld = new IntPosition(x, y, z);
          //add rock layer
          if (--rockLayer > 0)
          {
            SetBlock(coordsInWorld, typeof(RockBlock));
            continue;
          }
          //add grass layer
          if (--grassLayer > 0)
          {
            SetBlock(coordsInWorld, typeof(GrassBlock));
            continue;
          }
          //fill the rest with void
          SetBlock(coordsInWorld, typeof(VoidBlock));
        }

      }
    }
  }

  /// <summary>
  /// Checks whether the player moved to another chunk and loads world in that direction.
  /// </summary>
  private void LoadNewChunksIfNeeded()
  {
    IntPosition chunkMove = Helper.World2StorageCoords(PlayerPosition) - centerChunk;
    chunkMove.y = 0;
    if ( chunkMove != new IntPosition(0,0,0))
    {
      IntPosition dir = new IntPosition(0, 0, 0);
      if (chunkMove == dir.North)
      {
        MoveVisibleWorld(Direction.North);
        return;
      }
      if (chunkMove == dir.North + dir.East)
      {
        MoveVisibleWorld(Direction.North);
        MoveVisibleWorld(Direction.East);
        return;
      }
      if (chunkMove == dir.East)
      {
        MoveVisibleWorld(Direction.East);
        return;
      }
      if (chunkMove == dir.South + dir.East)
      {
        MoveVisibleWorld(Direction.South);
        MoveVisibleWorld(Direction.East);
        return;
      }
      if (chunkMove == dir.South)
      {
        MoveVisibleWorld(Direction.South);
        return;
      }
      if (chunkMove == dir.South + dir.West)
      {
        MoveVisibleWorld(Direction.South);
        MoveVisibleWorld(Direction.West);
        return;
      }
      if (chunkMove == dir.West)
      {
        MoveVisibleWorld(Direction.West);
        return;
      }
      if (chunkMove == dir.North + dir.West)
      {
        MoveVisibleWorld(Direction.North);
        MoveVisibleWorld(Direction.West);
        return;
      }
    }
  }

  /// <summary>
  /// Generates a wall of new chunks in a given direction.
  /// </summary>
  /// <param name="from"></param>
  /// <param name="to"></param>
  private void MoveVisibleWorld(Direction direction)
  {
    switch (direction)
    {
      case Direction.North:
        {
          int fromX = centerChunk.x - WorldRadius;
          int fromZ = centerChunk.z + WorldRadius;
          int toX = centerChunk.x + WorldRadius;
          int toZ = centerChunk.z + WorldRadius;
          CreateOrLoadChunkLine(fromX, fromZ, toX, toZ);
          centerChunk.z += 1;
        }
        break;
      case
        Direction.South:
        {
          int fromX = centerChunk.x - WorldRadius;
          int fromZ = centerChunk.z - WorldRadius;
          int toX = centerChunk.x + WorldRadius;
          int toZ = centerChunk.z - WorldRadius;
          CreateOrLoadChunkLine(fromX, fromZ, toX, toZ);
          centerChunk.z -= 1;
        }
        break;
      case Direction.East:
        {
          int fromX = centerChunk.x + WorldRadius;
          int fromZ = centerChunk.z - WorldRadius;
          int toX = centerChunk.x + WorldRadius;
          int toZ = centerChunk.z + WorldRadius;
          CreateOrLoadChunkLine(fromX, fromZ, toX, toZ);
          centerChunk.x += 1;
        }
        break;
      case Direction.West:
        {
          int fromX = centerChunk.x - WorldRadius;
          int fromZ = centerChunk.z - WorldRadius;
          int toX = centerChunk.x - WorldRadius;
          int toZ = centerChunk.z + WorldRadius;
          CreateOrLoadChunkLine(fromX, fromZ, toX, toZ);
          centerChunk.x -= 1;
        }
        break;
      default:
        throw new System.ArgumentException(string.Format("Invalid direction {0}. Must be North,South,East or West", direction.ToString()));

    }
  }

  private void CreateOrLoadChunkLine(int fromX, int fromZ, int toX, int toZ)
  {
    if (fromX != toX && fromZ != toZ  || (fromX == toX && fromZ == toZ ))
      throw new System.ArgumentException("coordinates must form a line along X or Z axis.");


    if (fromX == toX)
    {
      for (int z = fromZ; z < toZ; z++)
      {
        CreateOrActivateChunkColumn(toX, z);
      }
      return;
    }

    if (fromZ == toZ)
    {
      for (int x = fromX; x < toX; x++)
      {
        CreateOrActivateChunkColumn(x, toZ);
      }
      return;
    }
  }


  /// <summary>
  /// Activate chunks at given map coordinates. If there are none, creates new one with terrain.
  /// </summary>
  /// <param name="coords"></param>
  private void CreateOrActivateChunkColumn(int x, int z)
  {
    //check if there is already a chunk at given coordinates - if there is one at the bottom, the whole column is assumed to be loaded
    IntPosition botCoordsInStorage = new IntPosition(x, 0, z);
    Chunk bottomChunk;
    chunks.TryGetValue(botCoordsInStorage, out bottomChunk);
    if (bottomChunk == null)
    {
      //if no chunk was found, create new column
      for (int y = 0; y < WorldHeight; y++)
      {
        AddChunk(x, y, z);
      }
      CreateTerrainInColumn(x, z);
    }
    else
    {
      //if the chunks in the column are deactivated, activate them
      for (int y = 0; y < WorldHeight; y++)
      {
        IntPosition coordsInStorage = new IntPosition(x, y, z);
        Chunk chunk;
        chunks.TryGetValue(coordsInStorage, out chunk);
        if (!chunk.IsActive) chunk.Activate();
      }
    }
  }
}
