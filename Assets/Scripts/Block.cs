﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public abstract class Block
{
  public static float BlockSize = 1; // do not change this
  protected bool isTransparent = true;
  public bool IsTransparent { get { return isTransparent; } }
  protected bool isSolid = false;
  public bool IsSolid { get { return isSolid; } }
  public Chunk chunk;
  public IntPosition coordsInChunk; //position of block within the chunk

  public Block(Chunk chunk, IntPosition coordsInChunk)
  {
    this.chunk = chunk;
    this.coordsInChunk = coordsInChunk;
  }
  //adding vertices, triangles and uv mappings for renderig block faces
  public virtual void AddFaces() { }
  
  //adding vertices and triangles of face colliders
  public virtual void AddColliders() { }

  //adding uv mapping
  public virtual void AddUvMapping(){ }

}




