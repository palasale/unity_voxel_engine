﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores tile mapping information used by blocks for rendering.
/// </summary>
[RequireComponent(typeof(Material))]

public class TileMapper : MonoBehaviour {

  //we expect each tile to be of universal square shape and size
  private static float tileSize = 16/256f; //what percentage of the texture height/width does one tile occupy

  //positions of tiles in the texture are beginning from the bottom left corner
  private static Dictionary<FaceType, Vector2> tileCoords = new Dictionary<FaceType, Vector2>
  {
    { FaceType.RockOmnidirect, new Vector2(0,14) },
    { FaceType.GrassOmnidirect, new Vector2(2,15) },
    { FaceType.BedrockOmnidirect, new Vector2(1,14) }
  };
  //TODO: make this maping inspector friendly?


  /// <summary>
  /// Adds UV vertices of given face type to the given uv list.
  /// </summary>
  /// <param name="faceType"></param>
  /// <param name="uv"></param>
  public static void AddUvMapping(FaceType faceType, ref Chunk chunk)
  {
    Vector2 coord;
    if ( !tileCoords.TryGetValue(faceType, out coord) )
      throw new System.ArgumentException(string.Format("Block type \"{0}\" is not mapped to any texture", faceType.ToString()));

    chunk.AddUvCoord(new Vector2(tileSize * coord.x, tileSize * coord.y + tileSize));
    chunk.AddUvCoord(new Vector2(tileSize * coord.x + tileSize, tileSize * coord.y + tileSize));
    chunk.AddUvCoord(new Vector2(tileSize * coord.x + tileSize, tileSize * coord.y));
    chunk.AddUvCoord(new Vector2(tileSize * coord.x, tileSize * coord.y));
  }



}

public enum FaceType
{
  RockOmnidirect,
  GrassOmnidirect,
  BedrockOmnidirect
}
