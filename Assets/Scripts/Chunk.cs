﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

/// <summary>
/// Stores blocks and generates render and collider meshes for each block within.
/// </summary>
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(Material))]

[DebuggerDisplay ("coordsInWorld:({coordsInWorld.x},{coordsInWorld.y},{coordsInWorld.z})")]
public class Chunk : MonoBehaviour
{
  /// <summary>Number of blocks in length, width or height of a chunk.</summary>
  public static int ChunkSize = 16;
  private World world;
  public IntPosition coordsInWorld; //position of block that is at chunk coordinates [0,0,0]
  private Block[,,] blocks;

  private bool wasChanged;
  private bool isActive; //if set to false, the chunk will not draw its meshes
  public bool IsActive { get { return isActive; } }

  private List<Vector3> renderVertices;
  public int RenderVerticesCount { get { return renderVertices.Count; } }
  private List<int> renderTriangles;
  private List<Vector3> colVertices;
  public int ColVerticesCount { get { return colVertices.Count; } }

  private List<int> colTriangles;
  private List<Vector2> uv;

  private MeshRenderer meshRenderer;
  private MeshFilter meshFilter;
  private MeshCollider meshCollider;

  private void Start()
  {
    meshRenderer = GetComponent<MeshRenderer>();
    meshFilter = GetComponent<MeshFilter>();
    meshCollider = GetComponent<MeshCollider>();
    meshCollider.sharedMesh = null;
    meshCollider.sharedMesh = new Mesh(); //collider mesh needs to be initialized manually
    wasChanged = true;
  }
  private void Update()
  {
    if (wasChanged)
      CreateMeshes();
  }

  public void Init(World world, IntPosition coordsInWorld)
  {
    this.world = world;
    this.coordsInWorld = coordsInWorld;

    wasChanged = false;
    blocks = new Block[ChunkSize, ChunkSize, ChunkSize];
    renderVertices = new List<Vector3>();
    renderTriangles = new List<int>();
    colVertices = new List<Vector3>();
    colTriangles = new List<int>();
    uv = new List<Vector2>();

    FillWithVoid();
    isActive = true;
  }

  /// <summary>
  /// Fills all blocks in the chunk with the default block type.
  /// </summary>
  private void FillWithVoid()
  {
    for (int x = 0; x < ChunkSize; x++)
    {
      for (int y = 0; y < ChunkSize; y++)
      {
        for (int z = 0; z < ChunkSize; z++)
        {
          blocks[x, y, z] = new VoidBlock(this.GetComponent<Chunk>(), new IntPosition(x, y, z)); 
        }
      }
    }
    wasChanged = true;
  }

  /// <summary>
  /// Redraws render mesh, collider mesh and uv mapping for each block within this chunk.
  /// </summary>
  private void CreateMeshes()
  {
    if (IsActive)
    {
      for (int x = 0; x < ChunkSize; x++)
      {
        for (int y = 0; y < ChunkSize; y++)
        {
          for (int z = 0; z < ChunkSize; z++)
          {
            blocks[x, y, z].AddFaces();
            blocks[x, y, z].AddColliders();
          }
        }
      }
      meshFilter.mesh.Clear();
      meshFilter.mesh.vertices = renderVertices.ToArray();
      meshFilter.mesh.triangles = renderTriangles.ToArray();
      meshFilter.mesh.uv = uv.ToArray();
      meshFilter.mesh.RecalculateNormals();

      meshCollider.sharedMesh.Clear();
      meshCollider.sharedMesh.vertices = colVertices.ToArray();
      meshCollider.sharedMesh.triangles = colTriangles.ToArray();
      meshCollider.sharedMesh.RecalculateNormals();
      meshCollider.enabled = false; //TODO : debug why we need to turn meshCollider on and off at least once in order to make it work
      meshCollider.enabled = true;

      //Debug.Log("render vertice count: " + renderVertices.Count);
      //Debug.Log("collision vertice count: " + colVertices.Count);

      renderVertices.Clear();
      renderTriangles.Clear();
      colVertices.Clear();
      colTriangles.Clear();
      uv.Clear();

      wasChanged = false;
    }
  }

  /// <summary>
  /// Get block at chunk coordinates.
  /// </summary>
  /// <param name="coordsInChunk"></param>
  /// <returns></returns>
  public Block GetBlockFromChunk(IntPosition coordsInChunk)
  {
    //Debug.Log(string.Format("Looking for block at coordinates: {0},{1},{2}", worldPosition.x, worldPosition.y, worldPosition.z));
    
    //if coordinates do not belong to this chunk, tell World to find appropriate chunk
    if (coordsInChunk.x >= ChunkSize || coordsInChunk.x < 0 ||
        coordsInChunk.y >= ChunkSize || coordsInChunk.y < 0 ||
        coordsInChunk.z >= ChunkSize || coordsInChunk.z < 0 )
    {
      return world.GetBlock(coordsInChunk + this.coordsInWorld);
    }
    return blocks[coordsInChunk.x, coordsInChunk.y, coordsInChunk.z];
  }

  /// <summary>
  /// Get block at chunk coordinates.
  /// </summary>
  /// <param name="x"></param>
  /// <param name="y"></param>
  /// <param name="z"></param>
  /// <returns></returns>
  public Block GetBlockFromChunk(int x, int y, int z)
  {
    return GetBlockFromChunk(new IntPosition(x, y, z));
  }

  /// <summary>
  /// Sets a block in given coordinates within chunk.
  /// </summary>
  /// <param name="coordsInChunk"></param>
  /// <param name="newBlock"></param>
  public void SetBlock(IntPosition coordsInChunk, Block newBlock)
  {
    blocks[coordsInChunk.x, coordsInChunk.y, coordsInChunk.z] = newBlock;
    wasChanged = true;
  }

  /// <summary>
  /// Activates chunk and makes it to redraw itself in the next frame.
  /// </summary>
  public void Activate()
  {
    isActive = true;
    wasChanged = true;
  }

  /// <summary>
  /// Deactivates chunk to not draw its meshes.
  /// </summary>
  public void Deactivate()
  {
    isActive = false;
  }

  //---------------------Interface for adding render and collider info------------------------------------------
  public void AddRenderVertice(Vector3 vertice)
  {
    renderVertices.Add(vertice);
  }

  public void AddRenderTriangle(int index)
  {
    renderTriangles.Add(index);
  }

  public void AddColVertice(Vector3 vertice)
  {
    colVertices.Add(vertice);
  }

  public void AddColTriangle(int index)
  {
    colTriangles.Add(index);
  }

  public void AddUvCoord(Vector2 coord)
  {
    uv.Add(coord);
  }
  //-----------------------------------------End of interface ------------------------------------------

}

