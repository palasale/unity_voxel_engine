﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedrockBlock : SolidBlock
{
  public BedrockBlock(Chunk chunk, IntPosition coordsInChunk) : base(chunk, coordsInChunk, FaceType.BedrockOmnidirect)
  {
  }
}
