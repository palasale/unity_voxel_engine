﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassBlock : SolidBlock
{
  public GrassBlock(Chunk chunk, IntPosition coordsInChunk) : base(chunk, coordsInChunk, FaceType.GrassOmnidirect)
  {
  }
}
