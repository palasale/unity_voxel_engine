﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBlock : SolidBlock
{
  public RockBlock(Chunk chunk, IntPosition coordsInChunk) : base(chunk, coordsInChunk, FaceType.RockOmnidirect)
  {
  }
}
