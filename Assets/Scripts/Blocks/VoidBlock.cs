﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A simple empty and transparent block.
/// </summary>
public class VoidBlock : Block
{
  public VoidBlock(Chunk chunk, IntPosition position) : base(chunk, position)
  {
    base.isSolid = false;
    base.isTransparent = true;
  }
}
