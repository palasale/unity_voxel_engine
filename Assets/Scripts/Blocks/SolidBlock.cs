﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A simple solid block that draws collider and render meshes on sides that have no neighbours.
/// </summary>
public abstract class SolidBlock : Block
{
  public readonly FaceType faceType;
  public SolidBlock(Chunk chunk, IntPosition coordsInChunk, FaceType faceType) : base(chunk, coordsInChunk)
  {
    base.isTransparent = false;
    base.isSolid = true;
    this.faceType = faceType;
  }

  public override void AddFaces()
  {
    foreach (Direction direction in Enum.GetValues(typeof(Direction)))
    {

      AddRenderFace(direction);
    }
  }

  public override void AddColliders()
  {
    foreach (Direction direction in Enum.GetValues(typeof(Direction)))
    {
      AddColFace(direction);
    }
  }

  /// <summary>
  /// Adds appropriate vertices, triangles and uv mapping of a block face in given direction to the chunk.
  /// </summary>
  /// <param name="direction"></param>
  private void AddRenderFace(Direction direction)
  {
    int x = this.coordsInChunk.x;
    int y = this.coordsInChunk.y;
    int z = this.coordsInChunk.z;
    int lastVerticeIndex = chunk.RenderVerticesCount - 1;

    if (chunk.GetBlockFromChunk(coordsInChunk.Neighbour(direction)) == null || chunk.GetBlockFromChunk(coordsInChunk.Neighbour(direction)).IsTransparent)
    {
      switch (direction)
      {
        case Direction.North:
          {
            //create four vertices that will be used to create two triangles to compose a square face of a block
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y - BlockSize, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x, y, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x, y - BlockSize, z + BlockSize));

            AddRenderFaceTriangles(lastVerticeIndex);
            AddUvMapping();
          }
          break;
        case Direction.South:
          {
            chunk.AddRenderVertice(new Vector3(x, y - BlockSize, z));
            chunk.AddRenderVertice(new Vector3(x, y, z));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y, z));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y - BlockSize, z));

            AddRenderFaceTriangles(lastVerticeIndex);
            AddUvMapping();
          }
          break;
        case Direction.Up:
          {
            chunk.AddRenderVertice(new Vector3(x, y, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y, z));
            chunk.AddRenderVertice(new Vector3(x, y, z));

            AddRenderFaceTriangles(lastVerticeIndex);
            AddUvMapping();
          }
          break;
        case Direction.Down:
          {
            chunk.AddRenderVertice(new Vector3(x, y - BlockSize, z));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y - BlockSize, z));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y - BlockSize, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x, y - BlockSize, z + BlockSize));

            AddRenderFaceTriangles(lastVerticeIndex);
            AddUvMapping();
          }
          break;
        case Direction.West:
          {
            chunk.AddRenderVertice(new Vector3(x, y - BlockSize, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x, y, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x, y, z));
            chunk.AddRenderVertice(new Vector3(x, y - BlockSize, z));

            AddRenderFaceTriangles(lastVerticeIndex);
            AddUvMapping();
          }
          break;
        case Direction.East:
          {
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y - BlockSize, z));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y, z));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y, z + BlockSize));
            chunk.AddRenderVertice(new Vector3(x + BlockSize, y - BlockSize, z + BlockSize));

            AddRenderFaceTriangles(lastVerticeIndex);
            AddUvMapping();
          }
          break;
      }
    }
  }

  private void AddColFace(Direction direction)
  {
    int x = this.coordsInChunk.x;
    int y = this.coordsInChunk.y;
    int z = this.coordsInChunk.z;
    int lastVerticeIndex = chunk.ColVerticesCount - 1;

    if (chunk.GetBlockFromChunk(coordsInChunk.Neighbour(direction)) == null || !chunk.GetBlockFromChunk(coordsInChunk.Neighbour(direction)).IsSolid)
    {
      switch (direction)
      {
        case Direction.North:
          {
            //create four vertices that will be used to create two triangles to compose a square face of a block
            chunk.AddColVertice(new Vector3(x + BlockSize, y - BlockSize, z + BlockSize));
            chunk.AddColVertice(new Vector3(x + BlockSize, y, z + BlockSize));
            chunk.AddColVertice(new Vector3(x, y, z + BlockSize));
            chunk.AddColVertice(new Vector3(x, y - BlockSize, z + BlockSize));
            AddColFaceTriangles(lastVerticeIndex);
          }
          break;
        case Direction.South:
          {
            chunk.AddColVertice(new Vector3(x, y - BlockSize, z));
            chunk.AddColVertice(new Vector3(x, y, z));
            chunk.AddColVertice(new Vector3(x + BlockSize, y, z));
            chunk.AddColVertice(new Vector3(x + BlockSize, y - BlockSize, z));
            AddColFaceTriangles(lastVerticeIndex);
          }
          break;
        case Direction.Up:
          {
            chunk.AddColVertice(new Vector3(x, y, z + BlockSize));
            chunk.AddColVertice(new Vector3(x + BlockSize, y, z + BlockSize));
            chunk.AddColVertice(new Vector3(x + BlockSize, y, z));
            chunk.AddColVertice(new Vector3(x, y, z));
            AddColFaceTriangles(lastVerticeIndex);
          }
          break;
        case Direction.Down:
          {
            chunk.AddColVertice(new Vector3(x, y - BlockSize, z));
            chunk.AddColVertice(new Vector3(x + BlockSize, y - BlockSize, z));
            chunk.AddColVertice(new Vector3(x + BlockSize, y - BlockSize, z + BlockSize));
            chunk.AddColVertice(new Vector3(x, y - BlockSize, z + BlockSize));
            AddColFaceTriangles(lastVerticeIndex);
          }
          break;
        case Direction.West:
          {
            chunk.AddColVertice(new Vector3(x, y - BlockSize, z + BlockSize));
            chunk.AddColVertice(new Vector3(x, y, z + BlockSize));
            chunk.AddColVertice(new Vector3(x, y, z));
            chunk.AddColVertice(new Vector3(x, y - BlockSize, z));
            AddColFaceTriangles(lastVerticeIndex);
          }
          break;
        case Direction.East:
          {
            chunk.AddColVertice(new Vector3(x + BlockSize, y - BlockSize, z));
            chunk.AddColVertice(new Vector3(x + BlockSize, y, z));
            chunk.AddColVertice(new Vector3(x + BlockSize, y, z + BlockSize));
            chunk.AddColVertice(new Vector3(x + BlockSize, y - BlockSize, z + BlockSize));
            AddColFaceTriangles(lastVerticeIndex);
          }
          break;
      }
    }
  }
  private void AddRenderFaceTriangles(int lastVerticeIndex)
  {
    //each three entries of vertice index define one triangle
    //in this case the next four vertices after the last one define the face
    //we have to define it in a clockwise order to define the face direction
    chunk.AddRenderTriangle(lastVerticeIndex + 1); //bottom right vertice for the first triangle
    chunk.AddRenderTriangle(lastVerticeIndex + 2); //bottom left
    chunk.AddRenderTriangle(lastVerticeIndex + 3); //top left
    chunk.AddRenderTriangle(lastVerticeIndex + 1); //bottom right again for the second triangle
    chunk.AddRenderTriangle(lastVerticeIndex + 3); //top left
    chunk.AddRenderTriangle(lastVerticeIndex + 4); //top right
  }

  private void AddColFaceTriangles(int lastVerticeIndex)
  {
    chunk.AddColTriangle(lastVerticeIndex + 1); //bottom right vertice for the first triangle
    chunk.AddColTriangle(lastVerticeIndex + 2); //bottom left
    chunk.AddColTriangle(lastVerticeIndex + 3); //top left
    chunk.AddColTriangle(lastVerticeIndex + 1); //bottom right again for the second triangle
    chunk.AddColTriangle(lastVerticeIndex + 3); //top left
    chunk.AddColTriangle(lastVerticeIndex + 4); //top right
  }

  public override void AddUvMapping()
  {
    TileMapper.AddUvMapping(faceType, ref chunk);
  }
  
}
